var searchData=
[
  ['nunit_203_2e11_20_2d_20october_2011_2c_202018',['NUnit 3.11 - October 11, 2018',['../md__c_1__users__hergendy_source_repos__o_e__p_r_o_g4_packages__n_unit_83_811_80__c_h_a_n_g_e_s.html',1,'']]],
  ['newgame',['NewGame',['../class__02___game_logic_1_1_game_logic.html#ac2f64ed37f09dd0fcff454a43cbd05b3',1,'_02_GameLogic::GameLogic']]],
  ['newgamebutton',['NewGameButton',['../class_o_e___p_r_o_g4_1_1_view_model_1_1_main_view_model.html#a252dbf2225324115c627f9e697c1d33a',1,'OE_PROG4::ViewModel::MainViewModel']]],
  ['newgamecommand',['NewGameCommand',['../class_o_e___p_r_o_g4_1_1_view_model_1_1_main_view_model.html#ace0b68b81a23ab6ca0e8fad1e634a0dd',1,'OE_PROG4::ViewModel::MainViewModel']]],
  ['newgamestart',['NewGameStart',['../class__02___game_logic_1_1_game_logic.html#a8978abed45d3c1ddc099895eee05cb70',1,'_02_GameLogic::GameLogic']]],
  ['newgameworking',['NewGameWorking',['../class__05___tests_1_1_logic_tests.html#a986808bae78178262d053faf3e9f27eb',1,'_05_Tests::LogicTests']]],
  ['newlevel',['NewLevel',['../class__02___game_logic_1_1_game_logic.html#a8d187f1e5a9af722f4f21ec47f7c9c5a',1,'_02_GameLogic::GameLogic']]],
  ['newprofile',['NewProfile',['../class__02___game_logic_1_1_game_logic.html#a65bd7d6d553de2b88447ccfaa2f251f6',1,'_02_GameLogic::GameLogic']]],
  ['newprofilecommand',['NewProfileCommand',['../class_o_e___p_r_o_g4_1_1_view_model_1_1_main_view_model.html#a1756532b6c5ec2d2fdec2f9a26bfc3ae',1,'OE_PROG4::ViewModel::MainViewModel']]],
  ['newprofileworking',['NewProfileWorking',['../class__05___tests_1_1_logic_tests.html#ad72b48dd461a82a5063385a350b6b2d5',1,'_05_Tests::LogicTests']]]
];
