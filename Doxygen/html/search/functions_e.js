var searchData=
[
  ['readhighscoresfromxml',['ReadHighScoresFromXML',['../class__04___database_1_1_helpers_1_1_high_score_reader.html#a25d4621990fde89d1e5d1d8059ab771b',1,'_04_Database::Helpers::HighScoreReader']]],
  ['readplayersfromxml',['ReadPlayersFromXml',['../class__04___database_1_1_helpers_1_1_player_reader.html#af09ae1c17de46eb51c462f388c485122',1,'_04_Database::Helpers::PlayerReader']]],
  ['relaycommand',['RelayCommand',['../class_o_e___p_r_o_g4_1_1_helpers_1_1_relay_command.html#ac31bb87db7d41e242c6a524aae01e236',1,'OE_PROG4.Helpers.RelayCommand.RelayCommand(Action&lt; object &gt; execute_function, Predicate&lt; object &gt; canexecute_function)'],['../class_o_e___p_r_o_g4_1_1_helpers_1_1_relay_command.html#a873631a0cf53189030991da7b97e627e',1,'OE_PROG4.Helpers.RelayCommand.RelayCommand(Action&lt; object &gt; execute_function)']]],
  ['remove',['Remove',['../class__03___repository_1_1_high_score_repository.html#a710d3aec83c95a75e5dff7f238d8b963',1,'_03_Repository.HighScoreRepository.Remove()'],['../class__03___repository_1_1_player_repository.html#a3e580b21e81a27347efe31d83c834851',1,'_03_Repository.PlayerRepository.Remove()'],['../interface__04___database_1_1_interfaces_1_1_i_repository.html#a612aa089eabcdd65bfdac677f52a7ebf',1,'_04_Database.Interfaces.IRepository.Remove()']]]
];
