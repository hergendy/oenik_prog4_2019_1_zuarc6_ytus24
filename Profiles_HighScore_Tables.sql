﻿If OBJECT_ID('HighScore', 'U') IS NOT NULL
Drop table HighScore;
If OBJECT_ID('Player', 'U') IS NOT NULL
Drop table Player;

Create table Player (
PlayerID int IDENTITY(1,1),
PlayerName varchar(20),
Primary Key(PlayerID)
)

Create table HighScore (
HighScoreID int IDENTITY(1,1),
PlayerName varchar(20),
HighestLevel int,
TotalScore int
Primary Key(HighScoreID)
)