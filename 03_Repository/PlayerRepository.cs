﻿// <copyright file="PlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _03_Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using _04_Database;
    using _04_Database.Helpers;
    using _04_Database.Interfaces;

    /// <summary>
    /// Repository for players
    /// </summary>
    public class PlayerRepository : Repository, IPlayerRepository
    {
        private HADBEntities dbentity = new HADBEntities();

        /// <summary>
        /// Returns all Player
        /// </summary>
        /// <returns>Players</returns>
        public IQueryable<Player> GetAll()
        {
            return this.dbentity.Set<Player>().AsQueryable();
        }

        /// <summary>
        /// Inserts a player
        /// </summary>
        /// <param name="entity">Player to insert</param>
        public void Insert(Player entity)
        {
            this.dbentity.Players.Add(entity);
            this.dbentity.SaveChanges();
        }

        /// <summary>
        /// Removes a player
        /// </summary>
        /// <param name="entity">Player to remove</param>
        public void Remove(Player entity)
        {
            this.dbentity.Players.Remove(entity);
            this.dbentity.SaveChanges();
        }

        /// <summary>
        /// Saves profiles
        /// </summary>
        /// <param name="players">Players to save</param>
        public void SaveProfiles(List<Player> players)
        {
            PlayerWriter.SavePlayersToXml(players);
        }

        /// <summary>
        /// Loads profiles
        /// </summary>
        /// <returns>List of players loaded</returns>
        public List<Player> LoadProfiles()
        {
            return PlayerReader.ReadPlayersFromXml();
        }
    }
}
