﻿// <copyright file="HighScoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _03_Repository
{
    using System.Collections.Generic;
    using System.Linq;
    using _04_Database;
    using _04_Database.Helpers;
    using _04_Database.Interfaces;

    /// <summary>
    /// Repository for highscores
    /// </summary>
    public class HighScoreRepository : Repository, IHighScoreRepository
    {
        private HADBEntities dbentity = new HADBEntities();

        /// <summary>
        /// Returns all highscores
        /// </summary>
        /// <returns>All highscores</returns>
        public IQueryable<HighScore> GetAll()
        {
            return this.dbentity.Set<HighScore>().AsQueryable();
        }

        /// <summary>
        /// Inserts a highscore
        /// </summary>
        /// <param name="entity">Highscore to insert</param>
        public void Insert(HighScore entity)
        {
            this.dbentity.HighScores.Add(entity);
            this.dbentity.SaveChanges();
            HighScore[] highScores = this.dbentity.Set<HighScore>().OrderByDescending(x => x.TotalScore).Take(20).ToArray<HighScore>();
            for (int i = 0; i < highScores.Length; i++)
            {
                highScores[i].HighScoreID = i + 1;
            }
        }

        /// <summary>
        /// Loads highscores
        /// </summary>
        /// <returns>Loaded highscores</returns>
        public List<HighScore> LoadHighScores()
        {
            return HighScoreReader.ReadHighScoresFromXML();
        }

        /// <summary>
        /// Removes a highscore from the repo
        /// </summary>
        /// <param name="entity">Highscore to remove</param>
        public void Remove(HighScore entity)
        {
            this.dbentity.HighScores.Remove(entity);
            this.dbentity.SaveChanges();
        }

        /// <summary>
        /// Saves highscores
        /// </summary>
        /// <param name="highScores">Highscores to save</param>
        public void SaveHighScores(List<HighScore> highScores)
        {
            HighScoreWriter.SaveHighScoresToXML(highScores);
        }
    }
}
