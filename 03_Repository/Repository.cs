﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _03_Repository
{
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// Repository base class
    /// </summary>
    public class Repository
    {
        private static string connectionString = "metadata=res://*/HADBEntities.csdl|res://*/HADBEntities.ssdl|res://*/HADBEntities.msl;provider=System.Data.SqlClient;provider connection string=&quot;data source=(LocalDB)\\MSSQLLocalDB;attachdbfilename=|DataDirectory|\\HADB.mdf;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework&quot;";

        /// <summary>
        /// Commenction
        /// </summary>
        /// <returns>Database Connection</returns>
        protected IDbConnection GetConnection()
        {
            return new SqlConnection(connectionString);
        }

        /// <summary>
        /// Connects to the db
        /// </summary>
        /// <param name="dbConnection">Database Connection</param>
        protected void Connect(IDbConnection dbConnection)
        {
            dbConnection.Open();
        }
    }
}
