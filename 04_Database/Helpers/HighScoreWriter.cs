﻿// <copyright file="HighScoreWriter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Helpers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Linq;

    /// <summary>
    /// Writing high scores to file
    /// </summary>
    public class HighScoreWriter
    {
        /// <summary>
        /// Actual method
        /// </summary>
        /// <param name="highScores">Highscores to write</param>
        public static void SaveHighScoresToXML(List<HighScore> highScores)
        {
            XDocument xd = new XDocument();
            xd.Add(new XElement("root"));
            foreach (HighScore highScore in highScores)
            {
                XElement element = new XElement("highscore");
                element.Add(new XAttribute("id", highScore.HighScoreID));
                element.Add(new XAttribute("playername", highScore.PlayerName));
                element.Add(new XAttribute("level", highScore.HighestLevel));
                element.Add(new XAttribute("score", highScore.TotalScore));
                xd.Root.Add(element);
            }

            if (!Directory.Exists(FolderPath.GetPlayerLoadSavePath))
            {
                Directory.CreateDirectory(FolderPath.GetPlayerLoadSavePath);
            }

            xd.Save(FolderPath.GetPlayerLoadSavePath + "\\HighScores.xml");
        }
    }
}
