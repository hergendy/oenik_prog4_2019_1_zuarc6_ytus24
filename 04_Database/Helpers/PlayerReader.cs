﻿// <copyright file="PlayerReader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Helpers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Reading player list from file
    /// </summary>
    public static class PlayerReader
    {
        /// <summary>
        /// Actual method
        /// </summary>
        /// <returns>Read players from xml</returns>
        public static List<Player> ReadPlayersFromXml()
        {
            string path = FolderPath.GetPlayerLoadSavePath + "\\Players.xml";
            if (File.Exists(path))
            {
                return XDocument.Load(path).Descendants("player").Select(node => new Player()
                {
                    PlayerName = node.Attribute("name").Value,
                    PlayerID = int.Parse(node.Attribute("id").Value)
                }).ToList();
            }
            else
            {
                return null;
            }
        }
    }
}
