﻿// <copyright file="FolderPath.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Helpers
{
    using System.IO;

    /// <summary>
    /// Providing folderpath string
    /// </summary>
    public class FolderPath
    {
        /// <summary>
        /// Gets a string for the player and highscore loading folder
        /// </summary>
        public static string GetPlayerLoadSavePath
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Players";
            }
        }
    }
}
