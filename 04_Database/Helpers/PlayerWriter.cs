﻿// <copyright file="PlayerWriter.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Helpers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Xml.Linq;

    /// <summary>
    /// Writing player list to file
    /// </summary>
    public static class PlayerWriter
    {
        /// <summary>
        /// Actual method
        /// </summary>
        /// <param name="players">Player list to write</param>
        public static void SavePlayersToXml(List<Player> players)
        {
            XDocument xd = new XDocument();
            xd.Add(new XElement("root"));
            foreach (Player player in players)
            {
                XElement element = new XElement("player");
                element.Add(new XAttribute("name", player.PlayerName));
                element.Add(new XAttribute("id", player.PlayerID));
                xd.Root.Add(element);
            }

            if (!Directory.Exists(FolderPath.GetPlayerLoadSavePath))
            {
                Directory.CreateDirectory(FolderPath.GetPlayerLoadSavePath);
            }

            xd.Save(FolderPath.GetPlayerLoadSavePath + "\\Players.xml");
        }
    }
}
