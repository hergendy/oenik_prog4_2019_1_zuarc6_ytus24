﻿// <copyright file="HighScoreReader.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Helpers
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    /// <summary>
    /// Reads highscore from file
    /// </summary>
    public class HighScoreReader
    {
        /// <summary>
        /// Actual method
        /// </summary>
        /// <returns>The list of high scores</returns>
        public static List<HighScore> ReadHighScoresFromXML()
        {
            string path = FolderPath.GetPlayerLoadSavePath + "\\HighScores.xml";
            if (File.Exists(path))
            {
                return XDocument.Load(path).Descendants("highscore").Select(node => new HighScore()
                {
                    HighScoreID = int.Parse(node.Attribute("id").Value),
                    PlayerName = node.Attribute("player").Value,
                    HighestLevel = int.Parse(node.Attribute("level").Value),
                    TotalScore = int.Parse(node.Attribute("score").Value),
                }).ToList();
            }
            else
            {
                return null;
            }
        }
    }
}
