﻿// <copyright file="IHighScoreRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for a highscore repository
    /// </summary>
    public interface IHighScoreRepository : IRepository<HighScore>
    {
        /// <summary>
        /// Save method
        /// </summary>
        /// <param name="highScores">Highscores to save</param>
        void SaveHighScores(List<HighScore> highScores);

        /// <summary>
        /// Load method
        /// </summary>
        /// <returns>Loaded highscore list</returns>
        List<HighScore> LoadHighScores();
    }
}
