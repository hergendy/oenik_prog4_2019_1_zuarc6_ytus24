﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Interfaces
{
    using System.Linq;

    /// <summary>
    /// Interface for repositories
    /// </summary>
    /// <typeparam name="TEntity">Type of repo</typeparam>
    public interface IRepository<TEntity>
    {
        /// <summary>
        /// Returns all entities in the repo
        /// </summary>
        /// <returns>All entities</returns>
        IQueryable<TEntity> GetAll();

        /// <summary>
        /// Inserts an entity to the repo
        /// </summary>
        /// <param name="entity">Entity to insert</param>
        void Insert(TEntity entity);

        /// <summary>
        /// Removes an entity from the repo
        /// </summary>
        /// <param name="entity">Entity to remove</param>
        void Remove(TEntity entity);
    }
}
