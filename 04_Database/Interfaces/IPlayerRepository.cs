﻿// <copyright file="IPlayerRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _04_Database.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for player repository
    /// </summary>
    public interface IPlayerRepository : IRepository<Player>
    {
        /// <summary>
        /// Save profiles method
        /// </summary>
        /// <param name="players">profiles to save</param>
        void SaveProfiles(List<Player> players);

        /// <summary>
        /// Load profiles method
        /// </summary>
        /// <returns>Loaded profiles</returns>
        List<Player> LoadProfiles();
    }
}
