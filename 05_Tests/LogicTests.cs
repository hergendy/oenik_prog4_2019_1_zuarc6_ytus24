﻿// <copyright file="LogicTests.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _05_Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Windows.Input;
    using _02_GameLogic;
    using _02_GameLogic.Model;
    using _04_Database;
    using _04_Database.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Tests of GameLogic
    /// </summary>
    [TestFixture]
    public class LogicTests
    {
        private Mock<IPlayerRepository> mockPlayer;
        private Mock<IHighScoreRepository> mockHighScore;
        private bool isGameModeChangedInvoked = false;
        private bool isNewGameStartInvoked = false;
        private bool isGameCompletedInvoked = false;
        private GameLogic gL;

        /// <summary>
        /// Setup of tests
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.mockPlayer = new Mock<IPlayerRepository>();
            this.mockHighScore = new Mock<IHighScoreRepository>();

            List<Player> players = new List<Player>()
            {
                new Player() { PlayerID = 1, PlayerName = "Andras" },
                new Player() { PlayerID = 2, PlayerName = "Béla" },
                new Player() { PlayerID = 3, PlayerName = "Cecil" }
            };

            List<HighScore> highScores = new List<HighScore>()
            {
                new HighScore() { HighScoreID = 1, PlayerName = players[0].PlayerName, HighestLevel = 7, TotalScore = 1500 },
                new HighScore() { HighScoreID = 2, PlayerName = players[1].PlayerName, HighestLevel = 5, TotalScore = 1100 },
                new HighScore() { HighScoreID = 3, PlayerName = players[2].PlayerName, HighestLevel = 4, TotalScore = 800 }
            };

            this.mockPlayer.Setup(m => m.GetAll()).Returns(players.AsQueryable);
            this.mockHighScore.Setup(m => m.GetAll()).Returns(highScores.AsQueryable);
            this.mockPlayer.Setup(x => x.Insert(It.IsAny<Player>())).Callback(() =>
            {
                players.Add(new Player());
            });
            this.gL = new GameLogic(this.mockPlayer.Object, this.mockHighScore.Object);
            this.gL.SelectProfile(players[0]);
        }

        /// <summary>
        /// Testing of setting up the player repository
        /// </summary>
        [Test]
        public void PlayerRepoSetupWorking()
        {
            Assert.That(this.gL.PlayerRepository, Is.Not.Null);
            Assert.That(this.gL.PlayerRepository.GetAll().Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Testing of setting up the highscore repository
        /// </summary>
        [Test]
        public void HighscoreRepoSetupWorking()
        {
            Assert.That(this.gL.HighScoreRepository, Is.Not.Null);
            Assert.That(this.gL.HighScoreRepository.GetAll().Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Testing of adding a new profile
        /// </summary>
        [Test]
        public void NewProfileWorking()
        {
            this.gL.NewProfile("Dani");
            Assert.That(this.gL.PlayerRepository.GetAll().Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// Testing selecing a new profile
        /// </summary>
        [Test]
        public void SelectProfileWorking()
        {
            this.gL.SelectProfile(this.gL.PlayerRepository.GetAll().Last());
            string helper = GameLogic.CurrentPlayer.PlayerName;
            Assert.That(helper.Contains("Cecil"));
        }

        /// <summary>
        /// Testing of setting the game mode
        /// </summary>
        [Test]
        public void SetGameModeWorking()
        {
            GameLogic.GameModeChanged += this.GameLogic_GameModeChanged;

            this.gL.SetGameMode("Medium");
            Assert.That(this.gL.Difficulty, Is.EqualTo(Difficulty.Medium));
            Assert.That(this.gL.Distance, Is.EqualTo(6));
            Assert.That(this.isGameModeChangedInvoked, Is.True);
        }

        /// <summary>
        /// Testing the setting of distance
        /// </summary>
        [Test]
        public void SetDistanceWorking()
        {
            this.gL.SetGameMode("Inferno");
            Assert.That(this.gL.Distance, Is.EqualTo(4));
        }

        /// <summary>
        /// Testing the passing of seconds
        /// </summary>
        [Test]
        public void SecondPassedWorking()
        {
            for (int i = 0; i < 50; i++)
            {
                this.gL.SecondPassed();
            }

            Assert.That(this.gL.Seconds, Is.EqualTo(50));
            Assert.That(this.gL.Minutes, Is.EqualTo(0));

            for (int i = 0; i < 20; i++)
            {
                this.gL.SecondPassed();
            }

            Assert.That(this.gL.Seconds, Is.EqualTo(10));
            Assert.That(this.gL.Minutes, Is.EqualTo(1));
        }

        /// <summary>
        /// Testing of starting a new game
        /// </summary>
        [Test]
        public void NewGameWorking()
        {
            GameLogic.NewGameStart += this.GameLogic_NewGameStart;
            this.gL.NewGame();
            Assert.That(this.isNewGameStartInvoked, Is.True);
        }

        /// <summary>
        /// Testing of the end of a game
        /// </summary>
        [Test]
        public void EndGameWorking()
        {
            GameLogic.GameCompleted += this.GameLogic_GameCompleted;
            this.gL.NewGame();
            this.gL.EndGame();
            Assert.That(this.gL.Level, Is.EqualTo(0));
            Assert.That(this.gL.Seconds, Is.EqualTo(0));
            Assert.That(this.gL.Minutes, Is.EqualTo(0));
            Assert.That(this.isGameCompletedInvoked, Is.True);
        }

        /// <summary>
        /// Testing the pressing and releasing of a key
        /// </summary>
        [Test]
        public void KeyDownAndKeyUpWorking()
        {
            this.gL.Speed = 2;

            this.gL.KeyDown(Key.W);
            Assert.That(this.gL.PlayerSpeed_Y, Is.EqualTo(-2));
            this.gL.KeyUp(Key.W);
            Assert.That(this.gL.PlayerSpeed_Y, Is.EqualTo(0));

            this.gL.KeyDown(Key.S);
            Assert.That(this.gL.PlayerSpeed_Y, Is.EqualTo(2));
            this.gL.KeyUp(Key.S);
            Assert.That(this.gL.PlayerSpeed_Y, Is.EqualTo(0));

            this.gL.KeyDown(Key.A);
            Assert.That(this.gL.PlayerSpeed_X, Is.EqualTo(-2));
            this.gL.KeyUp(Key.A);
            Assert.That(this.gL.PlayerSpeed_X, Is.EqualTo(0));

            this.gL.KeyDown(Key.D);
            Assert.That(this.gL.PlayerSpeed_X, Is.EqualTo(2));
            this.gL.KeyUp(Key.D);
            Assert.That(this.gL.PlayerSpeed_X, Is.EqualTo(0));
        }

        private void GameLogic_NewGameStart(object sender, EventArgs e)
        {
            this.isNewGameStartInvoked = true;
        }

        private void GameLogic_GameCompleted(object sender, EventArgs e)
        {
            this.isGameCompletedInvoked = true;
        }

        private void GameLogic_GameModeChanged(object sender, EventArgs e)
        {
            this.isGameModeChangedInvoked = true;
        }
    }
}
