﻿// <copyright file="Difficulty.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _02_GameLogic.Model
{
    /// <summary>
    /// Difficulty selection for the game
    /// </summary>
    public enum Difficulty
    {
        /// <summary>
        /// Representing easy difficulty
        /// </summary>
        Easy,

        /// <summary>
        /// Representing medium difficulty
        /// </summary>
        Medium,

        /// <summary>
        /// Representing hard difficulty
        /// </summary>
        Hard,

        /// <summary>
        /// Representing inferno difficulty
        /// </summary>
        Inferno
    }
}
