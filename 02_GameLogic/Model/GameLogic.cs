﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _02_GameLogic
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading;
    using System.Windows.Input;
    using _02_GameLogic.Helpers;
    using _02_GameLogic.Model;
    using _03_Repository;
    using _04_Database;
    using _04_Database.Interfaces;

    /// <summary>
    /// Enumeration of game elements
    /// </summary>
    public enum GameElement
    {
        /// <summary>
        /// Representing a wall
        /// </summary>
        Wall,

        /// <summary>
        /// Representing a path
        /// </summary>
        Path,

        /// <summary>
        /// Representing an exit
        /// </summary>
        Exit,

        /// <summary>
        /// Representing a treasure
        /// </summary>
        Treasure,

        /// <summary>
        /// Representing darkness
        /// </summary>
        Darkness
    }

    /// <summary>
    /// Main logic of the game
    /// </summary>
    public class GameLogic : Bindable
    {
        private static readonly object SpeedLockValue = new object();
        private static IPlayerRepository playerRepository;
        private static IHighScoreRepository highScoreRepository;
        private static Difficulty difficulty;
        private static char[,] elements;
        private static bool[,] foWmask;
        private static int level = 0;
        private static int distance = 0;
        private static int speed = 0;
        private static IMaze maze;
        private static Player currentPlayer = null;
        private static int tileWidth = 0;
        private static int tileHeight = 0;
        private static int seconds = 0;
        private static int minutes = 0;
        private static int score = 0;
        private static bool gameInProgress = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// Parameterless constructor
        /// </summary>
        public GameLogic()
        {
            this.Player_X = 1;
            this.Player_Y = 1;
            this.PlayerDiff_X = 10;
            this.PlayerDiff_Y = 10;
            GameLogic.playerRepository = new PlayerRepository();
            GameLogic.highScoreRepository = new HighScoreRepository();
            this.LoadProfiles();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="playerRepository">Player Repo</param>
        /// <param name="highScoreRepository">Highscore Repo</param>
        public GameLogic(IPlayerRepository playerRepository, IHighScoreRepository highScoreRepository)
        {
            GameLogic.playerRepository = playerRepository;
            GameLogic.highScoreRepository = highScoreRepository;
            this.Player_X = 1;
            this.Player_Y = 1;
            this.PlayerDiff_X = 10;
            this.PlayerDiff_Y = 10;
            this.LoadProfiles();
        }

        /// <summary>
        /// Change of profile list event
        /// </summary>
        public static event EventHandler ProfileChanged;

        /// <summary>
        /// Change of current player event
        /// </summary>
        public static event EventHandler CurrentPlayerChanged;

        /// <summary>
        /// Change of highscore list event
        /// </summary>
        public static event EventHandler ScoreListChanged;

        /// <summary>
        /// Change of game mode event
        /// </summary>
        public static event EventHandler GameModeChanged;

        /// <summary>
        /// Change of game state event
        /// </summary>
        public static event EventHandler GameStateChanged;

        /// <summary>
        /// Starting a new game event
        /// </summary>
        public static event EventHandler NewGameStart;

        /// <summary>
        /// Ending a game event
        /// </summary>
        public static event EventHandler GameCompleted;

        /// <summary>
        /// Starting a game event
        /// </summary>
        public static event EventHandler GameStarted;

        /// <summary>
        /// Gets current player property
        /// </summary>
        public static Player CurrentPlayer
        {
            get
            {
                return currentPlayer;
            }

            private set
            {
                currentPlayer = value;
            }
        }

        /// <summary>
        /// Gets a value indicating whether game has started
        /// </summary>
        public static bool GameInProgress
        {
            get
            {
                return GameLogic.gameInProgress;
            }

            private set
            {
                GameLogic.gameInProgress = value;
            }
        }

        /// <summary>
        /// Gets Highscore list
        /// </summary>
        public ObservableCollection<HighScore> ScoreList
        {
            get
            {
                return new ObservableCollection<HighScore>(this.HighScoreRepository.GetAll().OrderByDescending(x => x.HighScoreID).Take(20));
            }
        }

        /// <summary>
        /// Gets the players list
        /// </summary>
        public ObservableCollection<Player> PlayerList
        {
            get
            {
                return new ObservableCollection<Player>(this.PlayerRepository.GetAll());
            }
        }

        /// <summary>
        /// Gets Difficulty
        /// </summary>
        public Difficulty Difficulty
        {
            get
            {
                return GameLogic.difficulty;
            }

            private set
            {
                GameLogic.difficulty = value;
                this.OnPropertyChange(nameof(GameLogic.difficulty));
            }
        }

        /// <summary>
        /// Gets or sets Fog of War mask for fog of waring the map
        /// </summary>
        public bool[,] FoWmask
        {
            get
            {
                return foWmask;
            }

            set
            {
                foWmask = value;
            }
        }

        /// <summary>
        /// Gets the elements of the maze
        /// </summary>
        public char[,] Elements
        {
            get
            {
                return GameLogic.elements;
            }

            private set
            {
                GameLogic.elements = value;
            }
        }

        /// <summary>
        /// Gets the X coordinate of the player
        /// </summary>
        public int Player_X { get; private set; }

        /// <summary>
        /// Gets the Y coordinate of the player
        /// </summary>
        public int Player_Y { get; private set; }

        /// <summary>
        /// Gets the X coordinate of the player difference
        /// </summary>
        public int PlayerDiff_X { get; private set; }

        /// <summary>
        /// Gets the Y coordinate of the player difference
        /// </summary>
        public int PlayerDiff_Y { get; private set; }

        /// <summary>
        /// Gets the X value of player speed
        /// </summary>
        public int PlayerSpeed_X { get; private set; }

        /// <summary>
        /// Gets the Y value of player speed
        /// </summary>
        public int PlayerSpeed_Y { get; private set; }

        /// <summary>
        /// Gets the distance of the camera
        /// </summary>
        public int Distance
        {
            get
            {
                return GameLogic.distance;
            }

            private set
            {
                GameLogic.distance = value;
            }
        }

        /// <summary>
        /// Gets or sets the height of a tile
        /// </summary>
        public int Tile_Height
        {
            get
            {
                return GameLogic.tileHeight;
            }

            set
            {
                GameLogic.tileHeight = value;
            }
        }

        /// <summary>
        /// Gets or sets the width of a tile
        /// </summary>
        public int Tile_Width
        {
            get
            {
                return GameLogic.tileWidth;
            }

            set
            {
                GameLogic.tileWidth = value;
            }
        }

        /// <summary>
        /// Gets or sets the speed of the player
        /// </summary>
        public int Speed
        {
            get
            {
                return GameLogic.speed;
            }

            set
            {
                GameLogic.speed = value;
            }
        }

        /// <summary>
        /// Gets the number of current level
        /// </summary>
        public int Level
        {
            get
            {
                return GameLogic.level;
            }

            private set
            {
                GameLogic.level = value;
                this.OnPropertyChange(nameof(level));
            }
        }

        /// <summary>
        /// Gets the seconds passed
        /// </summary>
        public int Seconds
        {
            get
            {
                return GameLogic.seconds;
            }

            private set
            {
                GameLogic.seconds = value;
                this.OnPropertyChange(nameof(seconds));
            }
        }

        /// <summary>
        /// Gets the minutes passed
        /// </summary>
        public int Minutes
        {
            get
            {
                return GameLogic.minutes;
            }

            private set
            {
                GameLogic.minutes = value;
                this.OnPropertyChange(nameof(minutes));
            }
        }

        /// <summary>
        /// Gets the player's score
        /// </summary>
        public int Score
        {
            get
            {
                return GameLogic.score;
            }

            private set
            {
                GameLogic.score = value;
                this.OnPropertyChange(nameof(score));
            }
        }

        /// <summary>
        /// Gets the speedlock object
        /// </summary>
        public object SpeedLock
        {
            get
            {
                return GameLogic.SpeedLockValue;
            }
        }

        /// <summary>
        /// Gets the Player Repository
        /// </summary>
        public IPlayerRepository PlayerRepository
        {
            get
            {
                return GameLogic.playerRepository;
            }
        }

        /// <summary>
        /// Gets the highscore repository
        /// </summary>
        public IHighScoreRepository HighScoreRepository
        {
            get
            {
                return GameLogic.highScoreRepository;
            }
        }

        /// <summary>
        /// Calculates the current area around the player
        /// </summary>
        /// <returns>GameElement array</returns>
        public GameElement[,] CurrentArea()
        {
            char current;
            int curr_x, curr_y;
            int size = (2 * this.Distance) + 3;
            GameElement[,] rst = new GameElement[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    curr_x = Clamper.Clamp(this.Player_X - this.Distance + i, 0, this.Elements.GetLength(0) - 1);
                    curr_y = Clamper.Clamp(this.Player_Y - this.Distance + j, 0, this.Elements.GetLength(1) - 1);
                    current = this.Elements[curr_x, curr_y];
                    if (this.FoWmask[curr_x, curr_y])
                    {
                        switch (current)
                        {
                            case '.':
                                rst[i, j] = GameElement.Path;
                                break;
                            case 't':
                                rst[i, j] = GameElement.Treasure;
                                break;
                            case 'x':
                                rst[i, j] = GameElement.Exit;
                                break;
                            default:
                            case '0':
                                rst[i, j] = GameElement.Wall;
                                break;
                        }
                    }
                    else
                    {
                        rst[i, j] = GameElement.Darkness;
                    }
                }
            }

            return rst;
        }

        /// <summary>
        /// Runs when you end a game
        /// </summary>
        public void EndGame()
        {
            GameCompleted?.Invoke(this, null);
            this.SaveHighScore(GameLogic.CurrentPlayer.PlayerName, this.Level, this.Score, this.HighScoreRepository.GetAll().OrderByDescending(x => x.HighScoreID).FirstOrDefault() == null ? 1 : this.HighScoreRepository.GetAll().OrderByDescending(x => x.HighScoreID).First().HighScoreID + 1);
            this.OnPropertyChange(nameof(this.ScoreList));
            ScoreListChanged?.Invoke(this, null);
            GameLogic.GameInProgress = false;
            GameLogic.GameStarted?.Invoke(this, null);
            this.Level = 0;
            this.Seconds = 0;
            this.Minutes = 0;
        }

        /// <summary>
        /// Starting a new game eventtrigger
        /// </summary>
        public void Starting()
        {
            GameLogic.GameInProgress = true;
            GameLogic.GameStarted?.Invoke(this, null);
        }

        /// <summary>
        /// Saves a highscore to the repo
        /// </summary>
        /// <param name="playerName">name of the player</param>
        /// <param name="highestLevel">highest level reached</param>
        /// <param name="totalScore">total score</param>
        /// <param name="highScoreId">ID</param>
        public void SaveHighScore(string playerName, int highestLevel, int totalScore, int highScoreId)
        {
            this.HighScoreRepository.Insert(
                   new HighScore()
                   {
                       PlayerName = playerName,
                       HighestLevel = highestLevel,
                       HighScoreID = highScoreId,
                       TotalScore = totalScore
                   });
            this.OnPropertyChange(nameof(this.ScoreList));
            ScoreListChanged?.Invoke(this, null);
        }

        /// <summary>
        /// Makes movement happen
        /// </summary>
        public void Movement()
        {
            lock (this.SpeedLock)
            {
                this.PlayerDiff_X += this.PlayerSpeed_X;
                if (((this.PlayerDiff_X < 0 && this.Elements[this.Player_X - 1, this.Player_Y] == '0') || (this.PlayerDiff_X > this.Tile_Width / 2 && this.Elements[this.Player_X + 1, this.Player_Y] == '0'))
                    || (this.PlayerDiff_X < 0 && this.PlayerDiff_Y > this.Tile_Height / 2 && (this.Elements[this.Player_X + 1, this.Player_Y - 1] == '0'))
                    || (this.PlayerDiff_X > this.Tile_Width / 2 && this.PlayerDiff_Y > this.Tile_Height / 2 && this.Elements[this.Player_X - 1, this.Player_Y - 1] == '0'))
                {
                    this.PlayerDiff_X -= this.PlayerSpeed_X;
                }

                this.PlayerDiff_Y += this.PlayerSpeed_Y;
                if (((this.PlayerDiff_Y < 0 && this.Elements[this.Player_X, this.Player_Y - 1] == '0') || (this.PlayerDiff_Y > this.Tile_Height / 2 && this.Elements[this.Player_X, this.Player_Y + 1] == '0'))
                    || ((this.PlayerDiff_Y < 0 && this.PlayerDiff_X > this.Tile_Width / 2 && (this.Elements[this.Player_X + 1, this.Player_Y - 1] == '0'))
                    || (this.PlayerDiff_Y > this.Tile_Height / 2 && this.PlayerDiff_X > this.Tile_Width / 2 && this.Elements[this.Player_X + 1, this.Player_Y + 1] == '0')))
                {
                    this.PlayerDiff_Y -= this.PlayerSpeed_Y;
                }

                if (this.PlayerDiff_X > this.Tile_Width || this.PlayerDiff_X < 0)
                {
                    this.Player_X += this.PlayerDiff_X < 0 ? -1 : 1;
                    this.PlayerDiff_X += this.PlayerDiff_X < 0 ? this.Tile_Width : -this.Tile_Width;
                }

                if (this.PlayerDiff_Y > this.Tile_Height || this.PlayerDiff_Y < 0)
                {
                    this.Player_Y += this.PlayerDiff_Y < 0 ? -1 : 1;
                    this.PlayerDiff_Y += this.PlayerDiff_Y < 0 ? this.Tile_Height : -this.Tile_Height;
                }
            }

            if (this.Elements[this.Player_X, this.Player_Y] == 'x')
            {
                switch (this.Difficulty)
                {
                    case Difficulty.Easy:
                        this.Score += (int)(Math.Sqrt(this.Level) * 1 * 50);
                        break;
                    case Difficulty.Medium:
                        this.Score += (int)(Math.Sqrt(this.Level) * 2 * 50);
                        break;
                    case Difficulty.Hard:
                        this.Score += (int)(Math.Sqrt(this.Level) * 3 * 50);
                        break;
                    default:
                    case Difficulty.Inferno:
                        this.Score += (int)(Math.Sqrt(this.Level) * 5 * 50);
                        break;
                }

                this.Player_X = 1;
                this.Player_Y = 1;
                this.NewLevel();
            }

            if (this.Elements[this.Player_X, this.Player_Y] == 't')
            {
                this.Elements[this.Player_X, this.Player_Y] = '.';
                switch (this.Difficulty)
                {
                    case Difficulty.Easy:
                        this.Score += (int)Math.Sqrt(this.Level) * 1 * 100;
                        break;
                    case Difficulty.Medium:
                        this.Score += (int)Math.Sqrt(this.Level) * 2 * 100;
                        break;
                    case Difficulty.Hard:
                        this.Score += (int)Math.Sqrt(this.Level) * 3 * 100;
                        break;
                    default:
                    case Difficulty.Inferno:
                        this.Score += (int)Math.Sqrt(this.Level) * 5 * 100;
                        break;
                }
            }

            this.CastSight();
            GameLogic.GameStateChanged?.Invoke(this, null);
        }

        /// <summary>
        /// Runs every second while playing
        /// </summary>
        public void SecondPassed()
        {
            this.Seconds++;
            if (this.Seconds >= 60)
            {
                this.Seconds -= 60;
                this.Minutes++;
            }
        }

        /// <summary>
        /// Runs when pressed a key
        /// </summary>
        /// <param name="key">Key pressedd</param>
        public void KeyDown(Key key)
        {
            switch (key)
            {
                case Key.W:
                case Key.Up:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_Y = -this.Speed;
                    }

                    break;
                case Key.S:
                case Key.Down:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_Y = this.Speed;
                    }

                    break;
                case Key.A:
                case Key.Left:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_X = -this.Speed;
                    }

                    break;
                case Key.D:
                case Key.Right:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_X = this.Speed;
                    }

                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Runs when a key is no longer pressed
        /// </summary>
        /// <param name="key">Key that is no longer pressed</param>
        public void KeyUp(Key key)
        {
            switch (key)
            {
                case Key.W:
                case Key.Up:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_Y = this.PlayerSpeed_Y < 0 ? 0 : this.PlayerSpeed_Y;
                    }

                    break;
                case Key.S:
                case Key.Down:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_Y = this.PlayerSpeed_Y > 0 ? 0 : this.PlayerSpeed_Y;
                    }

                    break;
                case Key.A:
                case Key.Left:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_X = this.PlayerSpeed_X < 0 ? 0 : this.PlayerSpeed_X;
                    }

                    break;
                case Key.D:
                case Key.Right:
                    lock (this.SpeedLock)
                    {
                        this.PlayerSpeed_X = this.PlayerSpeed_X > 0 ? 0 : this.PlayerSpeed_X;
                    }

                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// Loads profiles from the repo
        /// </summary>
        public void LoadProfiles()
        {
            if (this.PlayerRepository.LoadProfiles() != null)
            {
                foreach (var p in this.PlayerRepository.LoadProfiles())
                {
                    this.PlayerList.Add(p);
                }
            }
        }

        /// <summary>
        /// Sets the distance of the camera
        /// </summary>
        public void SetDistance()
        {
            switch (this.Difficulty)
            {
                case Difficulty.Easy:
                    this.Distance = 7;
                    break;
                case Difficulty.Medium:
                    this.Distance = 6;
                    break;
                case Difficulty.Hard:
                    this.Distance = 5;
                    break;
                default:
                case Difficulty.Inferno:
                    this.Distance = 4;
                    break;
            }
        }

        /// <summary>
        /// Runs when a new game is started
        /// </summary>
        public void NewGame()
        {
            this.Score = 0;
            this.Level = 0;
            this.Seconds = 0;
            this.Minutes = 0;
            this.NewLevel();
            GameLogic.NewGameStart?.Invoke(this, null);
        }

        /// <summary>
        /// Runs on every new level
        /// </summary>
        public void NewLevel()
        {
            GameLogic.maze = new Maze(this.Difficulty, this.Level++);
            Thread t = new Thread(() => GameLogic.maze.GenerateMaze(), 50000000);
            t.Start();
            t.Join();
            this.Elements = GameLogic.maze.GetMaze;
            this.FoWmask = new bool[this.Elements.GetLength(0), this.Elements.GetLength(1)];
            for (int i = 0; i < this.FoWmask.GetLength(0); i++)
            {
                for (int j = 0; j < this.FoWmask.GetLength(1); j++)
                {
                    this.FoWmask[i, j] = false;
                }
            }
        }

        /// <summary>
        /// Adds a new player to the repo
        /// </summary>
        /// <param name="playerName">Name of the player</param>
        /// <returns>Boolean of the insertion's success</returns>
        public bool NewProfile(string playerName)
        {
            if (this.PlayerRepository.GetAll().Where(x => x.PlayerName.ToUpper() == playerName.ToUpper()).Count() == 0)
            {
                this.PlayerRepository.Insert(new Player()
                {
                    PlayerID = this.PlayerRepository.GetAll().OrderByDescending(x => x.PlayerID).FirstOrDefault() == null ? 1 : this.PlayerRepository.GetAll().OrderByDescending(x => x.PlayerID).First().PlayerID + 1,
                    PlayerName = playerName
                });
                GameLogic.ProfileChanged?.Invoke(this, null);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Selects a new player as current
        /// </summary>
        /// <param name="selectedPlayer">New player</param>
        public void SelectProfile(Player selectedPlayer)
        {
            GameLogic.CurrentPlayer = selectedPlayer;
            GameLogic.CurrentPlayerChanged?.Invoke(this, null);
        }

        /// <summary>
        /// Deletes a player from the repo and list
        /// </summary>
        /// <param name="selectedPlayer">Player to delete</param>
        public void DeleteProfile(Player selectedPlayer)
        {
            int id = -1;
            id = this.PlayerRepository.GetAll().Where(x => x.PlayerName.ToUpper() == selectedPlayer.PlayerName.ToUpper()).Single().PlayerID;
            if (id != -1)
            {
                this.PlayerRepository.Remove(this.PlayerRepository.GetAll().Where(x => x.PlayerID == id).Single());
            }

            GameLogic.ProfileChanged?.Invoke(this, null);
        }

        /// <summary>
        /// Sets the game mode
        /// </summary>
        /// <param name="gameMode">Chosen Gamemode</param>
        public void SetGameMode(string gameMode)
        {
            this.Difficulty = (Difficulty)Enum.Parse(typeof(Difficulty), gameMode);
            this.SetDistance();
            GameLogic.GameModeChanged?.Invoke(this, null);
        }

        /// <summary>
        /// Saves high scores in the repo
        /// </summary>
        public void SaveHighScores()
        {
            this.HighScoreRepository.SaveHighScores(this.ScoreList.ToList());
        }

        /// <summary>
        /// Saves players in the repo
        /// </summary>
        public void SaveProfiles()
        {
            this.PlayerRepository.SaveProfiles(this.PlayerList.ToList());
        }

        private void CastSight()
        {
            int cast_X = this.Player_X + (this.PlayerDiff_X > this.Tile_Width / 2 ? 1 : 0);
            int cast_Y = this.Player_Y + (this.PlayerDiff_Y > this.Tile_Height / 2 ? 1 : 0);
            this.FoWmask[cast_X, cast_Y] = true;
            bool p_X = true, p_Y = true, n_X = true, n_Y = true;
            for (int i = 1; i < this.Distance; i++)
            {
                if (p_X)
                {
                    this.FoWmask[this.Player_X + i, this.Player_Y] = true;
                    this.FoWmask[this.Player_X + i, this.Player_Y + 1] = true;
                    this.FoWmask[this.Player_X + i, this.Player_Y - 1] = true;
                    if (this.Elements[this.Player_X + i, this.Player_Y] == '0')
                    {
                        p_X = false;
                    }
                }

                if (p_Y)
                {
                    this.FoWmask[this.Player_X, this.Player_Y + i] = true;
                    this.FoWmask[this.Player_X - 1, this.Player_Y + i] = true;
                    this.FoWmask[this.Player_X + 1, this.Player_Y + i] = true;
                    if (this.Elements[this.Player_X, this.Player_Y + i] == '0')
                    {
                        p_Y = false;
                    }
                }

                if (n_X)
                {
                    this.FoWmask[this.Player_X - i, this.Player_Y] = true;
                    this.FoWmask[this.Player_X - i, this.Player_Y + 1] = true;
                    this.FoWmask[this.Player_X - i, this.Player_Y - 1] = true;
                    if (this.Elements[this.Player_X - i, this.Player_Y] == '0')
                    {
                        n_X = false;
                    }
                }

                if (n_Y)
                {
                    this.FoWmask[this.Player_X, this.Player_Y - i] = true;
                    this.FoWmask[this.Player_X - 1, this.Player_Y - i] = true;
                    this.FoWmask[this.Player_X + 1, this.Player_Y - i] = true;
                    if (this.Elements[this.Player_X, this.Player_Y - i] == '0')
                    {
                        n_Y = false;
                    }
                }
            }
        }
    }
}
