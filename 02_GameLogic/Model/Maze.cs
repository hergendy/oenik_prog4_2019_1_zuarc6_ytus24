﻿// <copyright file="Maze.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _02_GameLogic.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using _02_GameLogic.Helpers;

    public class Maze : Bindable, IMaze
    {
        private static Random r = new Random();                                                                                 // Random generator field

        public static int Height { get; private set; }                                                                          // Height of the maze(Y)

        public static int Width { get; private set; }                                                                           // Width of the maze(X)

        public char[,] GetMaze { get { return elemek; } }

        private const int Dirs = 4;                                                                                             // Directions from a point of the maze

        private static List<Csucs> remaining = new List<Csucs>();                                                               // Remaining vertices

        private static List<Csucs> done = new List<Csucs>();                                                                    // Already reached vertices

        private static int[,,] dir;                                                                                             // 3 dimensional array containing all the directions available from each point of the maze

        private static char[,] elemek;                                                                                          // 2 dimensional array containing every point of the maze showing what it is

        public Maze(Difficulty difficulty, int level)
        {
            switch (difficulty)
            {
                case Difficulty.Easy:
                    Width = 3 + (level * 1);
                    Height = 3 + (level * 1);
                    break;
                case Difficulty.Medium:
                    Width = 5 + (level * 2);
                    Height = 5 + (level * 2);
                    break;
                case Difficulty.Hard:
                    Width = 7 + (level * 3);
                    Height = 7 + (level * 3);
                    break;
                case Difficulty.Inferno:
                    Width = 9 + (level * 5);
                    Height = 9 + (level * 5);
                    break;
            }
            dir = new int[(2 * Width) + 1, (2 * Height) + 1, Dirs];
            elemek = new char[(2 * Width) + 1, (2 * Height) + 1];
        }

        public void GenerateMaze() // Generating the maze main algorythm
        {
            for (int y = 0; y < Height; y++)                                                                                    // Filling the Remaining List with all the possible vertices
            {
                for (int x = 0; x < Width; x++)
                {
                    Csucs uj = new Csucs(x, y);
                    remaining.Add(uj);
                }
            }

            for (int y = 0; y < (2 * Height) + 1; y++)                                                                          // Filling in every point of the maze as a wall as a starting point
            {
                for (int x = 0; x < (2 * Width) + 1; x++)
                {
                    elemek[x, y] = '0';
                }
            }

            Csucs start = remaining.Find(z => z.X == 0 && z.Y == 0);                                                            // Making the start point of generating the maze (0,0)
            LabDepthRek(ref remaining, start);                                                                                  // Calling the main recursive depth-first search algorythm
            if (remaining.Count > 0) throw new Exception($"Még van {remaining.Count} darab csúcs!!!");                          // Error-check
            elemek[2 * Height - 1, 2 * Width - 1] = 'x';
            int walls = 0;
            int spaces = 0;

            for (int y = 0; y < (2 * Height) + 1; y++)                                                                          // Counting walls and spaces in the maze
            {
                for (int x = 0; x < (2 * Width) + 1; x++)
                {
                    if (elemek[x, y] == '0') walls++;
                    else spaces++;
                }
            }

            int remove = walls / 100;                                                                                           // Removing 1% of the walls to make loops in the maze
            while (remove > 0)
            {
                int x_rem = 0;
                int y_rem = 0;
                if (r.Next(0, 2) == 0)                                                                                          // 50-50% chance for taking out a horizontal or vertical wall(a wall that opens a path horizontally or vertically)
                {                                                                                                               // VERTICAL WALL
                    x_rem = (r.Next(Width - 2) * 2) + 3;                                                                        // X coordinate of a vertical wall is supposed to be odd
                    y_rem = (r.Next(Height - 1) * 2) + 2;                                                                       // Y coordinate of a vertical wall is supposed to be even
                    if (elemek[x_rem, y_rem] == '0' && elemek[x_rem - 2, y_rem] == '0' && elemek[x_rem + 2, y_rem] == '0')
                    {
                        elemek[x_rem, y_rem] = '.';
                        dir[x_rem, y_rem - 1, 2] = 1;
                        dir[x_rem, y_rem + 1, 0] = 1;
                        remove--;
                    }
                }
                else
                {                                                                                                               // HORIZONTAL WALL
                    x_rem = (r.Next(Width - 1) * 2) + 2;                                                                        // X coordinate of a horizontal wall is supposed to be even
                    y_rem = (r.Next(Height - 2) * 2) + 3;                                                                       // Y coordinate of a horizontal wall is supposed to be odd
                    if (elemek[x_rem, y_rem] == '0' && elemek[x_rem, y_rem - 2] == '0' && elemek[x_rem, y_rem + 2] == '0')
                    {
                        elemek[x_rem, y_rem] = '.';
                        dir[x_rem - 1, y_rem, 1] = 1;
                        dir[x_rem + 1, y_rem, 3] = 1;
                        remove--;
                    }
                }
            }
        }

        static void LabDepthRek(ref List<Csucs> elek, Csucs g)
        {
            remaining.Remove(g);
            elemek[(g.X * 2) + 1, (g.Y * 2) + 1] = '.';
            done.Add(g);
            List<Csucs> szomszedok = GetSzomszedokGen(g);
            while (szomszedok.Count > 0)
            {
                Csucs next = szomszedok.ElementAt(r.Next(0, szomszedok.Count));
                if (remaining.Contains(next))
                {
                    szomszedok.Remove(next);
                    if (g.X == next.X)
                    {
                        if (g.Y < next.Y) // Felfele lépés
                        {
                            elemek[(g.X * 2) + 1, (g.Y * 2) + 2] = '.';
                            dir[(g.X * 2) + 1, (g.Y * 2) + 1, 0] = 1;
                            dir[(next.X * 2) + 1, (next.Y * 2) + 1, 1] = 1;
                        }
                        else // Lefele lépés
                        {
                            elemek[(g.X * 2) + 1, g.Y * 2] = '.';
                            dir[(g.X * 2) + 1, (g.Y * 2) + 1, 1] = 1;
                            dir[(next.X * 2) + 1, (next.Y * 2) + 1, 0] = 1;
                        }
                    }
                    else
                    {
                        if (g.X < next.X) // Jobbra lépés
                        {
                            elemek[(g.X * 2) + 2, (g.Y * 2) + 1] = '.';
                            dir[(g.X * 2) + 1, (g.Y * 2) + 1, 2] = 1;
                            dir[(next.X * 2) + 1, (next.Y * 2) + 1, 3] = 1;
                        }
                        else // Balra lépés
                        {
                            elemek[g.X * 2, (g.Y * 2) + 1] = '.';
                            dir[(g.X * 2) + 1, (g.Y * 2) + 1, 3] = 1;
                            dir[(next.X * 2) + 1, (next.Y * 2) + 1, 2] = 1;
                        }
                    }

                    LabDepthRek(ref elek, next);
                    if (r.Next(101) > 98)
                    {
                        elemek[done.Last().X * 2 + 1, done.Last().Y * 2 + 1] = 't';
                    }
                }
                else
                    szomszedok.Remove(next);
            }
        }

        private static List<Csucs> GetSzomszedokGen(Csucs g)
        {
            List<Csucs> lista = new List<Csucs>();
            lista.Add(remaining.Find(z => z.X == g.X && z.Y == g.Y + 1));
            lista.Add(remaining.Find(z => z.X == g.X && z.Y == g.Y - 1));
            lista.Add(remaining.Find(z => z.X == g.X + 1 && z.Y == g.Y));
            lista.Add(remaining.Find(z => z.X == g.X - 1 && z.Y == g.Y));
            return lista;
        }

        internal class Csucs
        {
            public int X { get; set; }
            public int Y { get; set; }
            public Csucs(int x, int y)
            {
                X = x;
                Y = y;
            }
        }

    }
}
