﻿// <copyright file="Clamper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _02_GameLogic.Helpers
{
    /// <summary>
    /// Helper class for clamping
    /// </summary>
    public class Clamper
    {
        /// <summary>
        /// Clamps the value between the boundaries
        /// </summary>
        /// <param name="value">value to clamp</param>
        /// <param name="min">Lower boundary</param>
        /// <param name="max">Upper boundary</param>
        /// <returns>Clamped value</returns>
        public static int Clamp(int value, int min, int max)
        {
            return value < min ? min : value > max ? max : value;
        }
    }
}