﻿// <copyright file="FolderPaths.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _02_GameLogic.Helpers
{
    using System.IO;

    /// <summary>
    /// Helper path to load pictures
    /// </summary>
    public class FolderPaths
    {
        /// <summary>
        /// Gets string for mainscreen
        /// </summary>
        public string GetMainScreen
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\Mainscreen.jpg";
            }
        }

        /// <summary>
        /// Gets string for DragonBreath
        /// </summary>
        public string GetDragonBreath
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\DragonBreath.png";
            }
        }

        /// <summary>
        /// Gets string for LoadingScreen
        /// </summary>
        public string GetLoadingScreen
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\LoadingScreen.png";
            }
        }

        /// <summary>
        /// Gets string for MenuBackground
        /// </summary>
        public string GetMenuBackground
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\Menubackground.jpg";
            }
        }

        /// <summary>
        /// Gets string for NewGameBackground
        /// </summary>
        public string GetNewGameBackground
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\NewGameBackground.png";
            }
        }

        /// <summary>
        /// Gets string for ProfilesBackground
        /// </summary>
        public string GetProfilesBackground
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\Profilesbackground.png";
            }
        }

        /// <summary>
        /// Gets string for HighScoresBackground
        /// </summary>
        public string GetHighScoresBackground
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\HighScoresBackground.png";
            }
        }

        /// <summary>
        /// Gets string for CreepyDarkBackground
        /// </summary>
        public string GetCreepyDarkBackground
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\CreepyDarkBackground.png";
            }
        }

        /// <summary>
        /// Gets string for ProfilesListBackground
        /// </summary>
        public string GetProfilesListBackground
        {
            get
            {
                return Directory.GetCurrentDirectory() + "\\Pictures\\ProfilesListBackground.png";
            }
        }
    }
}