﻿// <copyright file="IMaze.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace _02_GameLogic.Helpers
{
    using System;

    /// <summary>
    /// Interface of the maze
    /// </summary>
    public interface IMaze
    {
        /// <summary>
        /// Gets Elements of the maze
        /// </summary>
        char[,] GetMaze { get; }

        /// <summary>
        /// Generates a maze on call
        /// </summary>
        void GenerateMaze();
    }
}
