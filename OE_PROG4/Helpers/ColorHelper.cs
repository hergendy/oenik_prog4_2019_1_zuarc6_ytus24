﻿// <copyright file="ColorHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OE_PROG4.Helpers
{
    using System.Windows.Media;

    /// <summary>
    /// Helper class for colors
    /// </summary>
    public class ColorHelper
    {
        /// <summary>
        /// Gets Transparent
        /// </summary>
        public Color GetAlphaZero
        {
            get { return Color.FromArgb(0, 0, 0, 0); }
        }

        /// <summary>
        /// Gets Red
        /// </summary>
        public SolidColorBrush GetFontRed
        {
            get { return new SolidColorBrush(Color.FromArgb(255, 237, 31, 4)); }
        }

        /// <summary>
        /// Gets Transparent Red
        /// </summary>
        public SolidColorBrush GetBackgroundTransparentRed
        {
            get { return new SolidColorBrush(Color.FromArgb(50, 224, 36, 11)); }
        }

        /// <summary>
        /// Gets Transparent Deep Purple
        /// </summary>
        public SolidColorBrush GetBackgroundTransparentDeepPurple
        {
            get { return new SolidColorBrush(Color.FromArgb(180, 73, 39, 86)); }
        }

        /// <summary>
        /// Gets Deep Purple
        /// </summary>
        public SolidColorBrush GetBackgroundDeepPurple
        {
            get { return new SolidColorBrush(Color.FromArgb(255, 73, 39, 86)); }
        }

        /// <summary>
        /// Gets a color for disabled buttons background that is supposed to be red
        /// </summary>
        public SolidColorBrush GetDisabledButtonBackgroundRed
        {
            get { return new SolidColorBrush(Color.FromArgb(150, 235, 76, 76)); }
        }

        /// <summary>
        /// Gets a color for disabled buttons background that is supposed to be gray
        /// </summary>
        public SolidColorBrush GetDisabledButtonForegroundGray
        {
            get { return new SolidColorBrush(Color.FromArgb(150, 40, 43, 67)); }
        }

        /// <summary>
        /// Gets a color for disabled buttons background that is supposed to be brown
        /// </summary>
        public SolidColorBrush GetEnabledButtonBackgroundBrown
        {
            get { return new SolidColorBrush(Color.FromArgb(255, 81, 12, 3)); }
        }
    }
}
