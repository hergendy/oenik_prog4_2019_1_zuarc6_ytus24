﻿// <copyright file="AnimationHelper.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OE_PROG4.Helpers
{
    using System;
    using System.Windows;
    using System.Windows.Media.Animation;

    /// <summary>
    /// Animation helper class for panels
    /// </summary>
    public static class AnimationHelper
    {
        /// <summary>
        /// Fades in the given dependency object.
        /// </summary>
        /// <param name="target">The target dependency object to fade in.</param>
        public static void FadeIn(DependencyObject target)
        {
            AnimateOpacity(target, 0, 1);
        }

        /// <summary>
        /// Expands/collapses the target dependency object
        /// </summary>
        /// <param name="target">The target dependency object to expand or collapse</param>
        /// <param name="from">Starting width</param>
        /// <param name="to">Finishing width</param>
        /// <param name="duration">Duration in milliseconds</param>
        public static void ExpandCollapse(DependencyObject target, double from, double to, double duration)
        {
            AnimateWidth(target, from, to, duration);
        }

        private static void AnimateOpacity(DependencyObject target, double from, double to)
        {
            var opacityAnimation = new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = TimeSpan.FromMilliseconds(500)
            };

            Storyboard.SetTarget(opacityAnimation, target);
            Storyboard.SetTargetProperty(opacityAnimation, new PropertyPath("Opacity"));

            var storyboard = new Storyboard();
            storyboard.Children.Add(opacityAnimation);
            storyboard.Begin();
        }

        private static void AnimateWidth(DependencyObject target, double from, double to, double duration)
        {
            DoubleAnimation widthAnimation = new DoubleAnimation
            {
                From = from,
                To = to,
                Duration = TimeSpan.FromMilliseconds(duration)
            };

            Storyboard.SetTarget(widthAnimation, target);
            Storyboard.SetTargetProperty(widthAnimation, new PropertyPath("Width"));
            Storyboard storyboard = new Storyboard();
            storyboard.Children.Add(widthAnimation);
            storyboard.Begin();
        }
    }
}
