﻿// <copyright file="IWindow.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OE_PROG4.Helpers
{
    using System.Windows.Controls;

    /// <summary>
    /// Interface for the gamelogic to access the mainwindow
    /// </summary>
    public interface IWindow
    {
        /// <summary>
        /// Gets the newgame stackpanel
        /// </summary>
        StackPanel GetNewGamePanel { get; }

        /// <summary>
        /// Gets the profiles stackpanel
        /// </summary>
        StackPanel GetProfilesPanel { get; }

        /// <summary>
        /// Gets the highscores stackpanel
        /// </summary>
        StackPanel GetHighScorePanel { get; }

        /// <summary>
        /// Gets the mainmenu stackpanel
        /// </summary>
        StackPanel GetMainMenuPanel { get; }

        /// <summary>
        /// Gets the loading screen grid
        /// </summary>
        Grid GetLoadingScreenGrid { get; }

        /// <summary>
        /// Gets the actual width of the window
        /// </summary>
        double GetActualWidth { get; }
    }
}
