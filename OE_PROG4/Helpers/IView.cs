﻿// <copyright file="IView.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OE_PROG4.Helpers
{
    using _02_GameLogic;

    /// <summary>
    /// Interface for mainviewmodel
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// Gets the Gamelogic of the viewmodel
        /// </summary>
        GameLogic GetGL { get; }
    }
}
