﻿// <copyright file="RelayCommand.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OE_PROG4.Helpers
{
    using System;
    using System.Windows.Input;

    /// <summary>
    /// Provided class button command
    /// </summary>
    public class RelayCommand : ICommand
    {
        private Action<object> executeFunction; // void(object)
        private Predicate<object> canexecuteFunction; // bool(object)

        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="execute_function">Function to execute</param>
        /// <param name="canexecute_function">Predicate of allowing to execute</param>
        public RelayCommand(
            Action<object> execute_function,
            Predicate<object> canexecute_function)
        {
            this.executeFunction = execute_function ??
                throw new ArgumentException("execute function not defined!");

            this.canexecuteFunction = canexecute_function ??
                throw new ArgumentException("can execute function not defined!");
        }

        /// <summary>
        /// Change of allowing of execution
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                // ha valaki feliratkozik
                CommandManager.RequerySuggested += value;
                this.CanExecuteChangedInternal += value;
            }

            remove
            {
                // ha valaki leiratkozik
                CommandManager.RequerySuggested -= value;
                this.CanExecuteChangedInternal -= value;
            }
        }

        private event EventHandler CanExecuteChangedInternal;

        // ha nem adunk meg canexecute-ot
        // akkor úgy vesszük hogy bármikor futhat a command

        /// <summary>
        /// Initializes a new instance of the <see cref="RelayCommand"/> class.
        /// </summary>
        /// <param name="execute_function">Function to execute</param>
        public RelayCommand(Action<object> execute_function)
            : this(execute_function, t => true)
        {
        }

        /// <summary>
        /// Checks if it can execute
        /// </summary>
        /// <param name="parameter">check parameter</param>
        /// <returns>True if it can execute</returns>
        public bool CanExecute(object parameter)
        {
            return this.canexecuteFunction != null
                && this.canexecuteFunction(parameter);
        }

        /// <summary>
        /// Executes the function
        /// </summary>
        /// <param name="parameter">Execute parameter</param>
        public void Execute(object parameter)
        {
            this.executeFunction?.Invoke(parameter);
        }

        /// <summary>
        /// Runs when canexecute has changed
        /// </summary>
        public void OnCanExecuteChanged()
        {
            EventHandler handler = this.CanExecuteChangedInternal;

            handler?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Destroys the command
        /// </summary>
        public void Destroy()
        {
            this.canexecuteFunction = t => false;
            this.executeFunction = t => { return; };
        }
    }
}
