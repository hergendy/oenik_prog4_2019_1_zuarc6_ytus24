﻿namespace OE_PROG4
{
    using System;
    using System.Text.RegularExpressions;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using OE_PROG4.Helpers;
    using OE_PROG4.ViewModel;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IWindow
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();
        }

        /// <summary>
        /// Gets the newgame stackpanel
        /// </summary>
        public StackPanel GetNewGamePanel
        {
            get
            {
                return this.NewGame;
            }
        }

        /// <summary>
        /// Gets the profiles stackpanel
        /// </summary>
        public StackPanel GetProfilesPanel
        {
            get
            {
                return this.Profiles;
            }
        }

        /// <summary>
        /// Gets the highscores stackpanel
        /// </summary>
        public StackPanel GetHighScorePanel
        {
            get
            {
                return this.HighScore;
            }
        }

        /// <summary>
        /// Gets the mainmenu stackpanel
        /// </summary>
        public StackPanel GetMainMenuPanel
        {
            get
            {
                return this.MainMenu;
            }
        }

        /// <summary>
        /// Gets the loading screen grid
        /// </summary>
        public Grid GetLoadingScreenGrid
        {
            get
            {
                return this.LoadingScreen;
            }
        }

        /// <summary>
        /// Gets the actual width of the window
        /// </summary>
        public double GetActualWidth
        {
            get
            {
                return this.ActualWidth;
            }
        }

        /// <summary>
        /// Validation method for allowed characters in textbox
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">Eventargs value</param>
        public void LetterValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^a-z]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            (this.FindResource("VM") as MainViewModel).SaveProfiles();
            (this.FindResource("VM") as MainViewModel).SaveHighScores();
        }
    }
}