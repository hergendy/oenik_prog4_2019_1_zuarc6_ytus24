﻿// <copyright file="GameArea.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OE_PROG4.ViewModel
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Windows;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Threading;
    using _02_GameLogic;

    /// <summary>
    /// Class for drawing the game area
    /// </summary>
    public class GameArea : FrameworkElement
    {
        // Using a DependencyProperty as the backing store for GL.  This enables animation, styling, binding, etc...

        /// <summary>
        /// Dependency property
        /// </summary>
        public static readonly DependencyProperty GLProperty = DependencyProperty.Register("MyProp", typeof(GameLogic), typeof(GameArea), new PropertyMetadata(null));

        private static readonly DispatcherTimer RefreshTimer = new DispatcherTimer(DispatcherPriority.Render);

        private static readonly DispatcherTimer TimerTimer = new DispatcherTimer(DispatcherPriority.Render);

        /// <summary>
        /// Gets or sets gamelogic dependency property
        /// </summary>
        public GameLogic GL
        {
            get
            {
                return (GameLogic)this.GetValue(GLProperty);
            }

            set
            {
                this.SetValue(GLProperty, value);
                GameLogic.GameStateChanged += this.GL_GameStateChanged;
                GameArea.RefreshTimer.Interval = new TimeSpan(0, 0, 0, 0, 5);
                GameArea.TimerTimer.Interval = new TimeSpan(0, 0, 1);
                GameArea.RefreshTimer.Tick += this.DispatcherTimer_Tick;
                GameArea.TimerTimer.Tick += this.TimerTimer_Tick;
                GameArea.RefreshTimer.Start();
                GameArea.TimerTimer.Start();
                this.GL.SetDistance();
                GameLogic.GameCompleted += this.GameLogic_GameCompleted;
            }
        }

        /// <summary>
        /// Renders a frame
        /// </summary>
        /// <param name="drawingContext">Something shit</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            this.GL.Tile_Height = (int)this.ActualHeight / ((this.GL.Distance * 2) + 1);
            this.GL.Tile_Width = (int)this.ActualWidth / ((this.GL.Distance * 2) + 1);
            this.GL.Speed = this.GL.Tile_Width / 30;
            GameElement[,] gameElements = this.GL.CurrentArea();
            drawingContext.DrawRectangle(Brushes.CadetBlue, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            for (int i = 0; i < gameElements.GetLength(0); i++)
            {
                for (int j = 0; j < gameElements.GetLength(1); j++)
                {
                    switch (gameElements[i, j])
                    {
                        case GameElement.Wall:
                            drawingContext.DrawRectangle(this.GetBrush("Wall1.png"), null, new Rect((i * this.GL.Tile_Width) - this.GL.PlayerDiff_X, (j * this.GL.Tile_Height) - this.GL.PlayerDiff_Y, this.GL.Tile_Width, this.GL.Tile_Height));
                            break;
                        case GameElement.Path:
                            drawingContext.DrawRectangle(this.GetBrush("Path1.png"), null, new Rect((i * this.GL.Tile_Width) - this.GL.PlayerDiff_X, (j * this.GL.Tile_Height) - this.GL.PlayerDiff_Y, this.GL.Tile_Width, this.GL.Tile_Height));
                            break;
                        case GameElement.Exit:
                            drawingContext.DrawRectangle(this.GetBrush("Door.png"), null, new Rect((i * this.GL.Tile_Width) - this.GL.PlayerDiff_X, (j * this.GL.Tile_Height) - this.GL.PlayerDiff_Y, this.GL.Tile_Width, this.GL.Tile_Height));
                            break;
                        case GameElement.Treasure:
                            drawingContext.DrawRectangle(this.GetBrush("Path1.png"), null, new Rect((i * this.GL.Tile_Width) - this.GL.PlayerDiff_X, (j * this.GL.Tile_Height) - this.GL.PlayerDiff_Y, this.GL.Tile_Width, this.GL.Tile_Height));
                            drawingContext.DrawRectangle(this.GetBrush("Treasure1.png"), null, new Rect((i * this.GL.Tile_Width) - this.GL.PlayerDiff_X + (this.GL.Tile_Width / 4), (j * this.GL.Tile_Height) - this.GL.PlayerDiff_Y + (this.GL.Tile_Height / 4), this.GL.Tile_Width / 2, this.GL.Tile_Height / 2));
                            break;
                        default:
                        case GameElement.Darkness:
                            drawingContext.DrawRectangle(Brushes.Black, null, new Rect((i * this.GL.Tile_Width) - this.GL.PlayerDiff_X, (j * this.GL.Tile_Height) - this.GL.PlayerDiff_Y, this.GL.Tile_Width, this.GL.Tile_Height));
                            break;
                    }
                }
            }

            drawingContext.DrawRectangle(this.GetBrush("Character.png"), null, new Rect((this.ActualWidth / 2) - (this.GL.Tile_Width / 2) - 5, (this.ActualHeight / 2) - (this.GL.Tile_Height / 2), this.GL.Tile_Width / 2, this.GL.Tile_Height / 2));
        }

        private void GameLogic_GameCompleted(object sender, EventArgs e)
        {
            GameArea.RefreshTimer.Tick -= this.DispatcherTimer_Tick;
            GameArea.TimerTimer.Tick -= this.TimerTimer_Tick;
        }

        private void TimerTimer_Tick(object sender, EventArgs e)
        {
            this.GL.SecondPassed();
        }

        private ImageBrush GetBrush(string filename)
        {
            return new ImageBrush(new BitmapImage(new Uri(Directory.GetCurrentDirectory() + "\\Sprites\\" + filename, UriKind.Absolute)));
        }

        private void DispatcherTimer_Tick(object sender, EventArgs e)
        {
            this.GL.Movement();
        }

        private Brush PickRandomBrush()
        {
            Brush result = Brushes.Transparent;

            Random rnd = new Random();

            Type brushesType = typeof(Brushes);

            PropertyInfo[] properties = brushesType.GetProperties();

            int random = rnd.Next(properties.Length);
            result = (Brush)properties[random].GetValue(null, null);

            return result;
        }

        private void GL_GameStateChanged(object sender, EventArgs e)
        {
            this.InvalidateVisual();
        }
    }
}
