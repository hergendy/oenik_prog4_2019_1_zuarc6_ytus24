﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace OE_PROG4.ViewModel
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using _02_GameLogic;
    using _02_GameLogic.Helpers;
    using _02_GameLogic.Model;
    using _03_Repository;
    using _04_Database;
    using _04_Database.Interfaces;
    using OE_PROG4.Helpers;

    /// <summary>
    /// MainViewModel class behind the mainwindow
    /// </summary>
    public class MainViewModel : Bindable
    {
        private readonly IPlayerRepository playerRepository = new PlayerRepository();
        private readonly IHighScoreRepository highScoreRepository = new HighScoreRepository();
        private readonly GameLogic gL;
        private readonly IWindow mainWindow;
        private int width = 0;
        private bool initialized = false;
        private StackPanel newGamePanel = new StackPanel();
        private StackPanel profilesPanel = new StackPanel();
        private StackPanel highScorePanel = new StackPanel();
        private StackPanel mainMenuPanel = new StackPanel();
        private Grid loadingScreenGrid = new Grid();

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
        {
            this.gL = new GameLogic(this.playerRepository, this.highScoreRepository);
            this.PlayerName = string.Empty;
            this.mainWindow = (IWindow)Application.Current.MainWindow;
            this.NewGameButton = new RelayCommand(this.NewGameButtonMethod);
            this.NewGameCommand = new RelayCommand(this.NewGameMethod, p1 => (this.CurrentPlayer != null && !this.GameInProgress));
            this.ProfilesButton = new RelayCommand(this.ProfilesButtonMethod);
            this.NewProfileCommand = new RelayCommand(this.NewProfileMethod, p2 => this.PlayerName.Length > 2);
            this.SelectProfileCommand = new RelayCommand(this.SelectProfileMethod, p3 => this.SelectedPlayerProfile != null);
            this.DeleteProfileCommand = new RelayCommand(this.DeleteProfileMethod, p4 => this.SelectedPlayerProfile != null);
            this.HighScoreButton = new RelayCommand(this.HighScoreButtonMetod);
            this.EasyGameModeButton = new RelayCommand(this.EasyGameModeMethod);
            this.MediumGameModeButton = new RelayCommand(this.MediumGameModeMethod);
            this.HardGameModeButton = new RelayCommand(this.HardGameModeMethod);
            this.InfernoGameModeButton = new RelayCommand(this.InfernoGameModeMethod);
            this.CollapseMenusButton = new RelayCommand(this.CollapseMenusButtonMethod, p => this.newGamePanel.Width > this.width || this.highScorePanel.Width > this.width || this.profilesPanel.Width > this.width);
            GameLogic.ProfileChanged += this.GL_ProfileChanged;
            GameLogic.CurrentPlayerChanged += this.GL_CurrentPlayerChanged;
            GameLogic.GameModeChanged += this.GL_GameModeChanged;
            GameLogic.NewGameStart += this.GL_NewGameStart;
            GameLogic.GameCompleted += this.GameLogic_GameCompleted;
            GameLogic.ScoreListChanged += this.GameLogic_ScoreListChanged;
            GameLogic.GameStarted += this.GameLogic_GameStarted;
        }

        /// <summary>
        /// Gets the current player's name
        /// </summary>
        public string GetCurrentPlayerName
        {
            get { return this.CurrentPlayer == null ? string.Empty : this.CurrentPlayer.PlayerName; }
        }

        /// <summary>
        /// Gets the currently selected game mode
        /// </summary>
        public string GetCurrentGameMode
        {
            get { return this.GameMode.ToString(); }
        }

        /// <summary>
        /// Gets Command for pressing the new game button
        /// </summary>
        public ICommand NewGameButton { get; private set; }

        /// <summary>
        /// Gets Command for starting a new game
        /// </summary>
        public ICommand NewGameCommand { get; private set; }

        /// <summary>
        /// Gets Command for pressing the profiles button
        /// </summary>
        public ICommand ProfilesButton { get; private set; }

        /// <summary>
        /// Gets Command for adding a new player
        /// </summary>
        public ICommand NewProfileCommand { get; private set; }

        /// <summary>
        /// Gets Command for selecting a player as current
        /// </summary>
        public ICommand SelectProfileCommand { get; private set; }

        /// <summary>
        /// Gets Command for deleting a player
        /// </summary>
        public ICommand DeleteProfileCommand { get; private set; }

        /// <summary>
        /// Gets Command for high score button
        /// </summary>
        public ICommand HighScoreButton { get; private set; }

        /// <summary>
        /// Gets command for choosing easy game mode
        /// </summary>
        public ICommand EasyGameModeButton { get; private set; }

        /// <summary>
        /// Gets command for choosing medium game mode
        /// </summary>
        public ICommand MediumGameModeButton { get; private set; }

        /// <summary>
        /// Gets command for choosing hard game mode
        /// </summary>
        public ICommand HardGameModeButton { get; private set; }

        /// <summary>
        /// Gets command for choosing inferno game mode
        /// </summary>
        public ICommand InfernoGameModeButton { get; private set; }

        /// <summary>
        /// Gets command for collapsing menus button
        /// </summary>
        public ICommand CollapseMenusButton { get; private set; }

        /// <summary>
        /// Gets the highscore list
        /// </summary>
        public ObservableCollection<HighScore> ScoreList
        {
            get { return new ObservableCollection<HighScore>(this.gL.ScoreList.OrderBy(x => x.HighScoreID).Take(20)); }
        }

        /// <summary>
        /// Gets the players list
        /// </summary>
        public ObservableCollection<Player> PlayerList
        {
            get { return this.gL.PlayerList; }
        }

        /// <summary>
        /// Gets or sets the selected player
        /// </summary>
        public Player SelectedPlayerProfile { get; set; }

        /// <summary>
        /// Gets the difficulty of the game
        /// </summary>
        public Difficulty GameMode
        {
            get { return this.gL.Difficulty; }
        }

        /// <summary>
        /// Gets or sets the name of the player to add
        /// </summary>
        public string PlayerName { get; set; }

        /// <summary>
        /// Gets the gamelogic
        /// </summary>
        public GameLogic GetGL
        {
            get { return this.gL; }
        }

        /// <summary>
        /// Gets the currently selected player
        /// </summary>
        private Player CurrentPlayer
        {
            get { return GameLogic.CurrentPlayer; }
        }

        private bool GameInProgress
        {
            get { return GameLogic.GameInProgress; }
        }

        /// <summary>
        /// Saves highscores
        /// </summary>
        public void SaveHighScores()
        {
            this.gL.SaveHighScores();
        }

        /// <summary>
        /// Saves profiles
        /// </summary>
        public void SaveProfiles()
        {
            this.gL.SaveProfiles();
        }

        private void GameLogic_ScoreListChanged(object sender, EventArgs e)
        {
            this.OnPropertyChange(nameof(this.ScoreList));
        }

        private void GameLogic_GameCompleted(object sender, EventArgs e)
        {
            AnimationHelper.ExpandCollapse(this.mainMenuPanel, 0, this.width, 1000);
            AnimationHelper.ExpandCollapse(this.highScorePanel, 0, 3 * this.width, 1000);
            AnimationHelper.ExpandCollapse(this.loadingScreenGrid, this.loadingScreenGrid.Width, 0, 1000);
            this.OnPropertyChange(nameof(this.GameInProgress));
        }

        private void GL_NewGameStart(object sender, EventArgs e)
        {
            GameWindow gW = new GameWindow();
            gW.ShowDialog();
        }

        private void GL_GameModeChanged(object sender, EventArgs e)
        {
            this.OnPropertyChange(nameof(this.GameMode));
            this.OnPropertyChange(nameof(this.GetCurrentGameMode));
        }

        private void GL_CurrentPlayerChanged(object sender, EventArgs e)
        {
            this.OnPropertyChange(nameof(this.CurrentPlayer));
            this.OnPropertyChange(nameof(this.GetCurrentPlayerName));
        }

        private void GL_ProfileChanged(object sender, EventArgs e)
        {
            this.OnPropertyChange(nameof(this.PlayerList));
        }

        private void InitializeWindow()
        {
            this.newGamePanel = this.mainWindow.GetNewGamePanel;
            this.profilesPanel = this.mainWindow.GetProfilesPanel;
            this.highScorePanel = this.mainWindow.GetHighScorePanel;
            this.mainMenuPanel = this.mainWindow.GetMainMenuPanel;
            this.loadingScreenGrid = this.mainWindow.GetLoadingScreenGrid;
            this.width = (int)this.mainWindow.GetActualWidth / 6;
            this.initialized = true;
        }

        private void GameLogic_GameStarted(object sender, EventArgs e)
        {
            this.OnPropertyChange(nameof(this.GameInProgress));
        }

        private void CollapseMenusButtonMethod(object obj)
        {
            AnimationHelper.ExpandCollapse(this.highScorePanel, this.highScorePanel.Width, this.width, 1000);
            AnimationHelper.ExpandCollapse(this.newGamePanel, this.newGamePanel.Width, this.width, 1000);
            AnimationHelper.ExpandCollapse(this.profilesPanel, this.profilesPanel.Width, this.width, 1000);
        }

        private void NewGameButtonMethod(object obj)
        {
            if (!this.initialized)
            {
                this.InitializeWindow();
            }

            if (this.newGamePanel.Width < 1.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.newGamePanel, this.newGamePanel.Width, 3 * this.width, 1000);
            }

            if (this.highScorePanel.Width > 2.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.highScorePanel, this.highScorePanel.Width, this.width, 1000);
            }

            if (this.profilesPanel.Width > 2.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.profilesPanel, this.profilesPanel.Width, this.width, 1000);
            }
        }

        private void HighScoreButtonMetod(object obj)
        {
            if (!this.initialized)
            {
                this.InitializeWindow();
            }

            if (this.highScorePanel.Width < 1.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.highScorePanel, this.highScorePanel.Width, this.width * 3, 1000);
            }

            if (this.newGamePanel.Width > 2.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.newGamePanel, this.newGamePanel.Width, this.width, 1000);
            }

            if (this.profilesPanel.Width > 2.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.profilesPanel, this.profilesPanel.Width, this.width, 1000);
            }
        }

        private void ProfilesButtonMethod(object obj)
        {
            if (!this.initialized)
            {
                this.InitializeWindow();
            }

            if (this.profilesPanel.Width < 1.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.profilesPanel, this.profilesPanel.Width, this.width * 3, 1000);
            }

            if (this.highScorePanel.Width > 2.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.highScorePanel, this.highScorePanel.Width, this.width, 1000);
            }

            if (this.newGamePanel.Width > 2.5 * this.width)
            {
                AnimationHelper.ExpandCollapse(this.newGamePanel, this.newGamePanel.Width, this.width, 1000);
            }
        }

        private void InfernoGameModeMethod(object obj)
        {
            this.gL.SetGameMode("Inferno");
        }

        private void HardGameModeMethod(object obj)
        {
            this.gL.SetGameMode("Hard");
        }

        private void MediumGameModeMethod(object obj)
        {
            this.gL.SetGameMode("Medium");
        }

        private void EasyGameModeMethod(object obj)
        {
            this.gL.SetGameMode("Easy");
        }

        private Task CollapseAll()
        {
            AnimationHelper.ExpandCollapse(this.profilesPanel, this.profilesPanel.Width, 0, 1000);
            AnimationHelper.ExpandCollapse(this.highScorePanel, this.highScorePanel.Width, 0, 1000);
            AnimationHelper.ExpandCollapse(this.newGamePanel, this.newGamePanel.Width, 0, 1000);
            AnimationHelper.ExpandCollapse(this.mainMenuPanel, this.mainMenuPanel.Width, 0, 1000);
            AnimationHelper.ExpandCollapse(this.loadingScreenGrid, this.profilesPanel.Width, this.width * 6, 1000);
            return Task.Delay(1100);
        }

        private async void NewGameMethod(object obj)
        {
            this.gL.Starting();
            await this.CollapseAll();
            this.gL.NewGame();
        }

        private void DeleteProfileMethod(object obj)
        {
            this.gL.DeleteProfile(this.SelectedPlayerProfile);
        }

        private void SelectProfileMethod(object obj)
        {
            this.gL.SelectProfile(this.SelectedPlayerProfile);
        }

        private void NewProfileMethod(object obj)
        {
            if (this.gL.NewProfile(this.PlayerName))
            {
                this.PlayerName = string.Empty;
                this.OnPropertyChange(nameof(this.PlayerName));
            }
        }
    }
}